from odoo import models, fields, api
class suscripcion(models.Model):
    _name = 'suscription.sale.order'
    partner_id       = fields.Many2one('res.partner',string="Cliente")
    parent_new_id    = fields.Many2one('res.partner',string="Empresa")
    type_contract_id = fields.Many2one('type.contract.yaros',string="Tipo de Contrato")
    order_id         = fields.Many2one('sale.order',string="orden_venta")
    #order_id = fields.One2many('')
    manager_id       = fields.Many2one('res.users',string="ejecutivo_venta")
    product_rayos_id = fields.Many2one('product.template',string="Producto")
    package          = fields.Char(string="paquete")
    not_etiquet      = fields.Boolean(string='no_generar_etiqueta')
    code             = fields.Char('Orden')
    recurring_rule_type = fields.Selection([("daily","Day(s)"),("weekly","Week(s)"),("monthly","Month(s)"),("yearly","Year(s)")])

    #pestaña informacion del contrato
    date_start     = fields.Date('fecha_inicio')
    date           = fields.Date('fecha_fin')
    quantity_rayos = fields.Char('cantidad')
    description     = fields.Text('Plazos_condiciones')

    #formas de entrega
    partner_contacts_id = fields.Many2one('res.partner','Contactos')
    route_yaros_id      = fields.Many2one('route.yaros','Rutas')
    day_week            = fields.Selection([('l','LUNES'),('m','MARTES'),('x','MIERCOLES'),
                                            ('j','JUEVES'),('v','VIERNES'),('s','SABADO'),('d','DOMINGO')])
    district_ids        = fields.Many2many('l10n_pe.res.city.district')
    format_delivery_id  = fields.Many2one('format.delivery.yaros','forma_entrega')
    sender_id     = fields.Many2one('res.partner','remitente_cortesia')
    courtesy      = fields.Char('cortesia')
    note          = fields.Char('Nota')
    etiquetas     = fields.One2many('sync.se.sesi.etiquetas','contract_id')


class Etiquetas(models.Model):
    _name = 'sync.se.sesi.etiquetas'
    orden = fields.Char()
    contract_id = fields.Many2one('suscription.sale.order','Contrato')
    salesorderid  = fields.Many2one('sale.order','so_code')
    numero_de_orden_interno = fields.Char('numero de orden interno')
    contact_id  = fields.Many2one('res.partner','Contacto')
    saludo      =  fields.Many2one('res.partner.title','Señor')
    nombre      = fields.Char()
    apellido    = fields.Char()
    cargo       = fields.Char()
    direccion   = fields.Char()
    distrito    = fields.Many2one('l10n_pe.res.city.district')
    provincia   = fields.Many2one('res.country.state')
    pais        = fields.Many2one('res.country')
    remitente   = fields.Char()
    fecha_registro = fields.Date()
    #grupo dos
    account_id  = fields.Char('Cuenta')
    empresa_name = fields.Char('Empresa')
    cantidad     = fields.Float()
    date_start     = fields.Date('fecha_inicio')
    date           = fields.Date('fecha_fin')
    state          = fields.Selection([('template','Plantilla'),('draft','Nuevo'),('open','En Pogreso'),
                                       ('pending','Para Renovar'),
                                        ('close','Cerrado'),('cancelled','Cancelado')])
    forma_entrega = fields.Char()
    product  = fields.Many2one('product.template')
    cod_product = fields.Char()
    prioridad = fields.Selection([('normal','Baja'),('urgent','Alta'),('very_urgent','Urgente')])
    route_yaros_id      = fields.Many2one('route.yaros','Rutas')
    tipo_revista = fields.Char()
    orden_entrega = fields.Char()
    categoria = fields.Char()
    propietario_cortesia  = fields.Char()
    sync = fields.Boolean('Sincronizado')

class TipoContrato(models.Model):
    _name = 'type.contract.yaros'
    name = fields.Char(required=True)

class Format(models.Model):
    _name          = 'format.delivery.yaros'
    name          = fields.Char(required=True)

class route(models.Model):
    _name          = 'route.yaros'
    name          = fields.Char(required=True)









